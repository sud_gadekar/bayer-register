import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { RegistrationComponent } from './registration/registration.component';
import { DataPolicyComponent } from './data-policy/data-policy.component';
import { TermsServiceComponent } from './terms-service/terms-service.component';
const routes: Routes = [
				{ path: '', component: RegistrationComponent},
				{ path: 'privacy-policy', component: PrivacyPolicyComponent},
				{ path: 'data-policy', component: DataPolicyComponent},
				{ path: 'terms-service', component: TermsServiceComponent},
				{ path: '**', component: RegistrationComponent }			
		];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
