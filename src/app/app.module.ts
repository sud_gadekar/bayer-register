import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { MatStepperModule,   } from '@angular/material/stepper';

import { AppComponent } from './app.component';
import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import { RecaptchaFormsModule} from 'ng-recaptcha';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import { AppRoutingModule } from './app-routing.module'; // CLI imports AppRoutingModule

import { RouterModule, Routes } from '@angular/router';
import {CdkStepperModule} from '@angular/cdk/stepper';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsServiceComponent } from './terms-service/terms-service.component';
import { DataPolicyComponent } from './data-policy/data-policy.component';
import { RegistrationComponent } from './registration/registration.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatStepperModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    MatButtonModule,
    MatInputModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    MatCheckboxModule,
    MatFormFieldModule,
    MatSelectModule,
    RouterModule.forRoot([]),
    CdkStepperModule
    
   
  ],
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    PrivacyPolicyComponent,
    TermsServiceComponent,
    DataPolicyComponent,
    RegistrationComponent,
  ],
  providers: [{
    provide: RECAPTCHA_SETTINGS,
    useValue: {
      siteKey: '6LfDZtAZAAAAAHKHMMglFkyZqev6xlJgdjdBoa2C',
    } as RecaptchaSettings,
  }],
  schemas: [
  CUSTOM_ELEMENTS_SCHEMA
],
})
export class AppModule { }


/**  Copyright 2018 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */