import { Component, ViewChild, ElementRef, HostListener, ChangeDetectorRef} from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl, FormArray} from '@angular/forms';
import { DataService } from '../shared-service/data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatStepper } from '@angular/material/stepper';
import { MAT_SELECT_SCROLL_STRATEGY } from '@angular/material/select';
import { Overlay, BlockScrollStrategy } from '@angular/cdk/overlay';
declare var $: any;

export function scrollFactory(overlay: Overlay): () => BlockScrollStrategy {
  return () => overlay.scrollStrategies.block();
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  providers: [
    { provide: MAT_SELECT_SCROLL_STRATEGY, useFactory: scrollFactory, deps: [Overlay] }
  ]
})
export class RegistrationComponent{

    @ViewChild('captchaRef', { static: false }) captchaRef: any;

    @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
        e.preventDefault();
    }
    @ViewChild('stepper') private myStepper: MatStepper;

    // @HostListener('document:keydown.tab', ['$event'])onKeydownHandler(event: KeyboardEvent) {
    //     event.preventDefault();
    // }



    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    thirdFormGroup: FormGroup;
    public loading: boolean = false;
    public selectPlaceholder;
    firstform = false;
    fieldTextType: boolean;
    spanShow: boolean = true

    isLinear = true;

    constructor(private FB: FormBuilder,
        private service: DataService,
        private toastr: ToastrService,
        private router: Router,
        private ref: ChangeDetectorRef) {

    }

    ngOnInit() {
        
        this.firstFormGroup = this.FB.group({
            salutation: [''],
            first_name: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[a-zA-Z ]*$')]],
            last_name: ['', [Validators.maxLength(20), Validators.pattern('^[a-zA-Z ]*$')]],
            country: ['', [Validators.required]],
            field_of_interest: ['', [Validators.required]],
            event_source: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[a-zA-Z ]*$')]],
            company_name: ['', [Validators.required]],
            job_title: ['', [Validators.required]],
        });
        this.secondFormGroup = this.FB.group({
            email: ['', [Validators.email, Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
            language: ['', [Validators.required]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirm_password: ['', [Validators.required, Validators.minLength(6)]],
        }, {
            validator: this.mustMatch
        });

        this.thirdFormGroup = this.FB.group({
            recaptchaReactive: ['', [Validators.required]],
            accept_terms: ['', [Validators.required]],
            privacy: ['', [Validators.required]],
            
        });


    }
    mustMatch(group: FormGroup) {
        let passControl = group.get('password');
        let confirmPassControl = group.get('confirm_password');
        if (confirmPassControl.value) {
            if (passControl.value === confirmPassControl.value) {
                if (confirmPassControl.hasError('notSame')) {
                    delete confirmPassControl.errors['notSame'];
                    confirmPassControl.updateValueAndValidity();
                }
                return null;
            } else {
                confirmPassControl.setErrors({
                    notSame: true
                });
                return {
                    notSame: true
                };
            }
        }
    }

    omit_special_char(event) {
        var k;
        k = event.charCode; //         k = event.keyCode;  (Both can be used)
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32);
    }

    omit_special_char_space(event) {
        var k;
        k = event.charCode; //         k = event.keyCode;  (Both can be used)
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8);
    }

    accept(event) {
        // console.log(event.checked);
        if (event.checked) {
            this.thirdFormGroup.patchValue({
                accept_terms: "true"
            });
        } else {
            this.thirdFormGroup.patchValue({
                accept_terms: ""
            })
        }
    }
    privacy(event) {
        // console.log(event.checked);
        if (event.checked) {
            this.thirdFormGroup.patchValue({
                privacy: "true"
            });
        } else {
            this.thirdFormGroup.patchValue({
                privacy: ""
            })
        }
    }

    async resolved(captchaResponse: string) {
        // console.log(`Resolved response token: ${captchaResponse}`);
        //await this.sendTokenToBackend(captchaResponse);
    }

    isControlHasError(controlName: string, validationType: string): boolean {
        const control = this.firstFormGroup.controls[controlName] || this.secondFormGroup.controls[controlName] || this.thirdFormGroup.controls[controlName];
        if (!control) {
            return false;
        }

        const result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    }

    // registration() {
    //     if (this.RegistrationForm.invalid) {
    //         return false;
    //     }
    //     let postData = this.RegistrationForm.value;
    //     postData.field_of_interest = this.RegistrationForm.get('field_of_interest').value.join(",");
    //     // //postData.field_of_interest = postData.field_of_interest.join(",");
    //     // console.log(postData);
    //     //  return false;
    //     this.loading = true;
    //     this.service.postRecord('attendee', postData).subscribe(res => {
    //         if (res.Status) {
    //             this.toastr.success(res.Msg, 'Success');
    //             this.loading = false;
    //             this.RegistrationForm.reset();
    //             this.captchaRef.reset();
    //             $('span.holder').removeClass('hidePlaceholder');
    //             $('span.holder').addClass('showPlaceholder');
    //             $('span.selectHolder').removeClass('hidePlaceholder');
    //             $('span.selectHolder').addClass('showPlaceholder');
    //         } else {
    //             this.toastr.error(res.Msg, 'Error', {
    //                 timeOut: 3000
    //             });
    //             this.loading = false;
    //             this.captchaRef.reset();
    //         }
    //     }, error => {
    //         this.toastr.error('Something went wrong. Please try again', 'Error', {
    //             timeOut: 3000,
    //         });
    //         this.loading = false;
    //         this.captchaRef.reset();
    //     });
    // }

    registration() {

        if (this.firstFormGroup.invalid || this.secondFormGroup.invalid || this.thirdFormGroup.invalid) {
            return false;
        }

        let postData = this.firstFormGroup.value;
        postData.plan_id = 1;
        postData.event_id = 2;

        postData.field_of_interest = this.firstFormGroup.get('field_of_interest').value.join(",");
        const registrationData = Object.assign({}, postData, this.secondFormGroup.value, this.thirdFormGroup.value, )
        this.loading = true;

        this.service.postRecord('attendee', registrationData).subscribe(res => {
            if (res.Status) {
                this.toastr.success(res.Msg, 'Success');
                this.loading = false;
                // this.firstFormGroup.reset();
                // this.secondFormGroup.reset();
                // this.thirdFormGroup.reset();
                // this.captchaRef.reset();
                // this.myStepper.reset();
                // $('span.holder').removeClass('hidePlaceholder');
                // $('span.holder').addClass('showPlaceholder');
                // $('span.selectHolder').removeClass('hidePlaceholder');
                // $('span.selectHolder').addClass('showPlaceholder');
                if (this.firstFormGroup.get('country').value == 'Switzerland') {
                    window.location.href = "https://head2toebayer.com/Switzerland/thank_you_page.html";
                } else if (this.firstFormGroup.get('country').value == 'Austria') {
                    window.location.href = "https://head2toebayer.com/Austria/thank_you_page.html";
                } else if (this.firstFormGroup.get('country').value == 'France') {
                    window.location.href = "https://head2toebayer.com/France/thank_you_page.html";
                } else
                {
                    window.location.href = "https://www.head2toebayer.com/thankyou.html";
                }
            } else {
                this.toastr.error(res.Msg, 'Error', {
                    timeOut: 3000
                });
                this.loading = false;
                this.captchaRef.reset();
            }
        }, error => {
            this.toastr.error('Something went wrong. Please try again', 'Error', {
                timeOut: 3000,
            });
            this.loading = false;
            this.captchaRef.reset();
        });
    }

    toggleFieldTextType() {
        this.fieldTextType = !this.fieldTextType;
    }

    selectP(placeholder, event) {
        this.selectPlaceholder = placeholder;
        if (event.value) {
            this.selectPlaceholder.classList.remove("showPlaceholder");
            this.selectPlaceholder.classList.add("hidePlaceholder");
            this.ref.markForCheck();
            this.ref.detectChanges();
        }
        this.ref.markForCheck();
        this.ref.detectChanges();
    }  

}
