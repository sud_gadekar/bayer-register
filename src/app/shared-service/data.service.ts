import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient,HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DataService {

	baseUrl = "https://www.head2toebayer.com/bayer-api/api/";

	//baseUrl = "https://webizexpo.com/virtual-expo-api/api/";

	private loading = new BehaviorSubject(false);
	loadingExport = this.loading.asObservable();

	constructor(private httpService: HttpClient) {
	}


	login(url: string, email, password): Observable < any > {

		return this.httpService.post(this.baseUrl + url, {
			email: email,
			password: password
		});
	}

	getProfile(): Observable < any > {
	const httpHeaders = new HttpHeaders({
       Authorization: `Bearer ${localStorage.getItem('accessToken')}`
     });
	return this.httpService.get < any > (this.baseUrl + 'me',{ headers: httpHeaders});
	}
	postRecord(url: string, data: any): Observable < any > {
		return this.httpService.post < any > (this.baseUrl + url, data);
	}
}
