/***********
Making placeholder star specifically red 
****************/


$(function() {


    $("span.holder + input").keyup(function() {
        if($(this).val().length) {
        	$(this).prev('span.holder').removeClass('showPlaceholder');
            $(this).prev('span.holder').addClass('hidePlaceholder');
        } else {
        	$(this).prev('span.holder').removeClass('hidePlaceholder');
            $(this).prev('span.holder').addClass('showPlaceholder');
        }
    });    
});